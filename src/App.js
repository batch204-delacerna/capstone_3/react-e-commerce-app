import './App.css';

// Packages / Libraries
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Swtich } from 'react-router-dom';

// Components
import AppNavBar from './components/AppNavBar';

// Pages
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';

import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // useEffect(() => {
  //   fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
  //     headers: {
  //       Authorization: `Bearer ${localStorage.getItem("token")}`
  //     }
  //   })
  //   .then(res => res.json())
  //   .then(data => {
  //     if(typeof data._id !== 'undefined'){
  //       setUser({
  //         id: data._id,
  //         isAdmin: data.isAdmin
  //       })
  //     }else{
  //       setUser({
  //         id: null,
  //         isAdmin: null
  //       })        
  //     }
  //   })
  // }, [])

  return (
    <>
      <AppNavBar/>
    </>
  );
}

export default App;
